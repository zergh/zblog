# Plan

1. Setup development environment.
2. Setup skeleton application.

## Important but not urgent

- [ ] Setup node configurations. [Reading list](https://github.com/lorenwest/node-config)
- [ ] Setup logging with request id and rotating file. [Readin list](https://github.com/winstonjs/winston-daily-rotate-file)
- [ ] Setup error logging with 'express global error handler'
- [ ] Switch from PUG to [Vue](https://www.vuemastery.com/courses/intro-to-vue-js/vue-instance/)
- [ ] [Nodejs Express tutorial](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs)

## 1. Setup development environment

### Install Node.js, web application framework and database

I use Express and MongoDB because they are more popular.
Fill package.json with full data,
include main entrypoint and avoid OS-related paths.

Reading list:

- [Node.js Hello World!](https://nodejs.org/en/docs/guides/getting-started-guide/)
- [Node.js web application frameworks](https://www.airpair.com/node.js/posts/nodejs-framework-comparison-express-koa-hapi)
- [Express development environment](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/mongoose)
- [Express database integrations](https://expressjs.com/en/guide/database-integration.html)
- [MongoDB with Mongoose](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment)

### Install development dependencies

I use these package because I like them.
Assign main and development dependencies appropriately.

- [node config](https://github.com/lorenwest/node-config)
- [gulp](https://gulpjs.com/)
- [eslint](https://eslint.org/)
  with [airbnb styleguide](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb)
- [bunyan](https://github.com/trentm/node-bunyan)
- [ava](https://github.com/avajs/ava)
  with [supertest](https://github.com/visionmedia/supertest) and [mongodb-memory-server](https://github.com/nodkz/mongodb-memory-server)
- [gulp-saas](https://www.npmjs.com/package/gulp-sass)
  with [stylelint](https://www.npmjs.com/package/stylelint) for future use

### Setup single api REST endpoint to check the environment

- [ ] TODO: add error handlings
- [ ] Clean obsolete code

### Cover the endpoint with tests and implement

todododo
