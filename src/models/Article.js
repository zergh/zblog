const mongoose = require('mongoose');
const slug = require('slugs');

const { Schema } = mongoose;
mongoose.Promise = global.Promise;


const articleSchema = new Schema({
  title: {
    type: String,
    trim: true,
    required: 'Please enter a post title!',
  },
  author: String,
  date: Date,
  slug: String,
  text: String,
  tags: [String],
  views: Number,
  comments: [String],
  scores: [Number],
});

articleSchema.pre('save', function preHook(next) {
  if (!this.slug) this.slug = slug(this.title);
  if (!this.isModified('title')) {
    next();
    return;
  }
  this.slug = slug(this.title);
  next();
  // TODO: Unique slugs
});

module.exports = mongoose.model('Article', articleSchema);
