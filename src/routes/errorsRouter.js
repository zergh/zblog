const express = require('express');
const errorsController = require('../controllers/errorsController.js');

const errorsRouter = express.Router();

module.exports = {
  register(app) {
    errorsRouter.get(errorsController.common);

    app.use(errorsRouter);
  },
};
