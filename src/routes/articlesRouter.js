const express = require('express');
const articlesController = require('../controllers/articlesController.js');

const articlesRouter = express.Router();

module.exports = {
  register(app) {
    articlesRouter.get('/articles', articlesController.articles_all_get);
    articlesRouter.get('/articles/top', articlesController.articles_topTen_get);
    articlesRouter.get('/articles/:slug', articlesController.article_detail_get);
    articlesRouter.post('/articles/new', articlesController.article_create_post);
    articlesRouter.put('/articles/:slug', articlesController.article_update_put);
    articlesRouter.delete('/articles/:slug', articlesController.article_delete);

    app.use('/api/blog', articlesRouter);
  },
};
