const env = process.env.NODE_ENV;

// For Mongodb validation errors
exports.flashValidationErrors = (err, req, res, next) => {
  if (err.errors) {
    // validation errors look like
    const errorKeys = Object.keys(err.errors);
    errorKeys.forEach(key => req.flash('error', err.errors[key].message));
    res.redirect('back');
  } else {
    next(err);
  }
};

exports.commonErrors = (err, req, res, next) => {
  if (err.errors) {
    res.status(err.status || 500);
    if (env === 'development') {
      err.message = '234' + err.message,
      res.send(err);
    } else {
      res.send({
        message: 'asd' + err.message,
        error: {},
      });
    }
  } else {
    const notFoundErr = new Error('ffff');
    res.status = 404;
    res.send(notFoundErr);
  }
};
