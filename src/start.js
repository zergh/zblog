const mongoose = require('mongoose');

// Import loggerInstance
const logger = require('pino')();


// Import enviroment variables
require('dotenv').config({ path: 'variables.env' });

// Connect to database
const options = { useNewUrlParser: true };
// log.info(process.env.DATABASE);

// mongoose.connect(process.env.DATABASE, options);
mongoose.connect('mongodb://localhost:27017/zblogdb', options);

mongoose.Promise = global.Promise;
mongoose.connection.on('error', (err) => {
  logger.error(`db error ${err.message}`);
});
mongoose.connection.on('connected', () => {
  logger.info(`Mongoose default connection open to ${process.env.DATABASE}`);
});
mongoose.connection.on('disconnected', () => {
  logger.info('Mongoose default connection disconnected');
});
process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    logger.info('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});


// Import models
// require('./models/Blogpost');

// Start application server
const app = require('./app');

app.set('port', process.env.PORT || 7777);
const server = app.listen(app.get('port'), () => {
  logger.info(`Express running -> PORT ${server.address().port}`);
});
