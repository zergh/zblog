const mongoose = require('mongoose');
const Article = mongoose.model('Article');


exports.articles_all_get = (async (req, res) => {
  const articles = await Article.find();
  res.json(articles);
  // res.render('entries', { title: 'Entries', entries });
});

exports.articles_page_get = (async (req, res) => {
  const articles = await Article
    .find()
    .skip((req.query.pageNumber - 1) * req.query.pageSize)
    .limit(req.query.pageSize);
  res.json(articles);
  // res.render('entries', { title: 'Entries', entries });
});

exports.articles_topTen_get = (async (req, res) => {
  const articles = await Article
    .aggregate()
    .project({ totalScore: { $sum: 'scores' } })
    .sort({ totalScore: 1 })
    .limit(10);
  res.json(articles);
  // res.render('entries', { title: 'Entries', entries });
});

exports.article_detail_get = (async (req, res) => {
  const article = await Article.findOne({ slug: req.params.slug }, { _id: 0, __v: 0 });
  if (article) {
    res.json(article);
  } else {
    res.status(404);
    res.send(Error(`No such article: ${req.params.slug}.`));
  }
  // res.render('entries', { title: 'Entries', entries });
});

exports.article_create_post = (async (req, res) => {
  const article = await new Article(req.body);
  article.save((err) => {
    if (err) res.send(err);
    res.json(article);
  });
});

exports.article_update_put = (async (req, res) => {
  await Article.findOneAndUpdate({ slug: req.params.id }, req.body, {
    new: true,
    runValidators: true,
  }).exec();
  if (err) res.send(err);
  const article = await Article.findOne({ slug: req.params.slug }, { _id: 0, __v: 0 });
  if (article) {
    res.json(article);
  }  
});

exports.article_delete = (async (req, res) => {
  await Article.findOneAndDelete({ slug: req.params.id }, req.body, {
    new: true,
    runValidators: true,
  }).exec();
  if (err) res.send(err);
  res.json(req.params.id);
});


// articleController.get = (async (req, res) => {
//   const article = await Article.findOne({ _id: req.params.id });
//   res.json(article)
//   //res.render('entries', { title: 'Entries', entries });
//   //logger.logResponse(req.id, res, 200);
// });

// articleController.post = (async (req, res) => {
//   await new Article(req.body).save();
// //  logger.logResponse(req.id, res, 201);
// });

// articleController.put = (async (req, res) => {
//   await Article.findOneAndUpdate({ _id: req.params.id }, req.body, {
//     new: true,
//     runValidators: true,
//   }).exec();
// });

// articleController.delete = (async (req, res) => {
//   await Article.findOneAndDelete({ _id: req.params.id }, req.body, {
//     new: true,
//     runValidators: true,
//   }).exec();
// });

// exports.homePage = (req, res) => {
//   res.render('index');
//   logger.logResponse(req.id, res, 200);
// };

// exports.addEntry = (req, res) => {
//   res.render('editEntry', { title: 'Add Article' });
//   logger.logResponse(req.id, res, 200);
// };

// exports.createEntry = async (req, res) => {
//   const entry = new Entry(req.body);
//   await entry.save();
//   req.flash('success', `Created a ${entry.entryTitle}.`);
//   res.redirect(`/blog/${entry.slug}`);
//   logger.logResponse(req.id, res, 200);
// };

// exports.getEntries = async (req, res) => {
//   const entries = await Entry.find();
//   //TODO: uncouple renders from backend calls
//   await res.json(entries)
//   //res.render('entries', { title: 'Entries', entries });
//   //logger.logResponse(req.id, res, 200);
// };

// exports.editEntry = async (req, res) => {
//   const entry = await Entry.findOne({ _id: req.params.id });
//   // TODO: confirm owner permissions
//   res.render('editEntry', { title: `Edit ${entry.entryTitle}`, entry });
//   logger.logResponse(req.id, res, 200);
// };

// exports.updateEntry = async (req, res) => {
//   const entry = await Entry.findOneAndUpdate({ _id: req.params.id }, req.body, {
//     new: true,
//     runValidators: true,
//   }).exec();
//   // req.flash('success', `Successfully updated ${entry.entryTitle}.
//   //   a href="/blog/${entry.slug}">View Post </a>`);
//   // TODO: replace mongoid with slug
//   res.redirect(`/blog/${entry._id}/edit`);
//   logger.logResponse(req.id, res, 200);
// };
