const env = process.env.NODE_ENV;

exports.common = (err, req, res, next) => {
  if (err.errors) {
    res.status(err.status || 500);
    if (env === 'development') {
      err.message = '234' + err.message,
      res.send(err);
    } else {
      res.send({
        message: 'asd' + err.message,
        error: {},
      });
    }
  } else {
    const notFoundErr = new Error('ffff');
    res.status = 404;
    res.send(notFoundErr);
  }
};
