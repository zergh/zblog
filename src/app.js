// Outer Imports

const express = require('express');
// const mongoose = require('mongoose');
// const path = require('path');
const bodyParser = require('body-parser');
// const session = require('express-session');
// const MongoStore = require('connect-mongo')(session);
// const flash = require('connect-flash');
const expressPinoLogger = require('express-pino-logger');

// Inner Imports

// Models
// eslint-disable-next-line no-unused-vars
const Article = require('./models/Article');
// eslint-disable-next-line no-unused-vars
const User = require('./models/User');

// const helpers = require('./helpers');
const errorHandlers = require('./handlers/errorHandlers');
// const logger = require('./logger');
const articleRoutes = require('./routes/articlesRouter');
const errorRoutes = require('./routes/errorsRouter');

// Express app
const app = express();
app.use(expressPinoLogger());

// Import enviroment variables
require('dotenv').config({ path: '../variables.env' });

// Sessions allow us to store data on visitors from request to request
// This keeps users logged in and allows us to send flash messages
// app.use(session({
//   secret: process.env.SECRET,
//   key: process.env.KEY,
//   resave: false,
//   saveUninitialized: false,
//   store: new MongoStore({ mongooseConnection: mongoose.connection }),
// }));


// raw requests into req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// view engine
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');

// scss processing
// var srcPath = path.join(__dirname, 'sass');
// var destPath = path.join(__dirname, '/public/styles');

// app.use('/styles', sassMiddleware({
//   src: srcPath,
//   dest: destPath,
//   debug: true,
//   outputStyle: 'expanded'
// }));

// public files
// app.use(express.static(path.join(__dirname, 'public')));

// flashing message middleware
// app.use(flash());

// common variables
// app.use((req, res, next) => {
//   res.locals.h = helpers;
//   res.locals.flashes = req.flash();
//   res.locals.user = req.user || null;
//   res.locals.currentPath = req.path;
//   next();
// });

// Error handler for mongo validation errors
// app.use(errorHandlers.flashValidationErrors);

// route handlers
articleRoutes.register(app);

// error handlers
app.use(errorHandlers.commonErrors);
errorRoutes.register(app);
// all done

module.exports = app;
