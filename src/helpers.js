const fs = require('fs');

exports.sitename = 'Sitename stuff';

exports.menu = [
  { slug: '/register', title: 'Register' },
  { slug: '/login', title: 'Register' },
];

// "Console.log" on page
exports.dump = obj => JSON.stringify(obj, null, 2);

// inserting an SVG
exports.icon = name => fs.readFileSync(`${__dirname}/public/icons/${name}.svg`);
