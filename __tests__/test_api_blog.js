// Libraries required for testing. Needs linter fix.
import test from 'ava';
import request from 'supertest';
// Bolerplate
import { before, after } from './utils';

const faker = require('faker');
const slug = require('slugs');
// Your models and server
const Article = require('../models/Article');
const app = require('../app');

test.before(before);

// SUITE

test('/articles :: get all articles :: success', async (t) => {
  const { app, testData } = t.context;
  const testArticle = faker.random.arrayElement(testData);
  const res = await request(app)
    .get('/api/blog/articles');
  t.is(res.status, 200);
  t.is(res.body.some(() => testArticle), true);
});

// test('/articles/top :: get 10 top rated articles :: success', async (t) => {
//   const { app, testData } = t.context;
//   const topArticles = testData.sort(
//     (a, b) => { return (
//       a.scores.reduce((total, num) => total + num),
//       b.scores.reduce((total, num) => total + num));
//     }
//   );
//   const topTenArticles = topArticles.slice(0, 9);
//   const res = await request(app)
//     .get('/api/blog/articles/top');
//   t.is(res.status, 200);
//   t.is(res.body.size, 10);
//   t.is(res.body, topTenArticles);
// });

test.todo('/articles?pageSize=10&pageNumber=3 :: get pagined articles :: success');

test.todo('/article :: post new article :: success');

test('/article/:id :: get article by slug :: success ', async (t) => {
  const { app, testData } = t.context;
  const testArticle = faker.random.arrayElement(testData);
  const url = `/api/blog/articles/${testArticle.slug}`;
  const res = await request(app)
    .get(url);
  t.is(res.status, 200);
  t.deepEqual(res.body, testArticle);
});

test.todo('/article/:id :: update existing articles :: success');

test.todo('/article/:id :: delete existing articles :: success');

// Clean up database after every test
test.afterEach.always((t) => Article.deleteMany());

//* ************************************************************************************************
//* ************************************************************************************************
//* ************************************************************************************************
//                                     SUITE BOILERPLATE
//* ************************************************************************************************
//* ************************************************************************************************
//* ************************************************************************************************
test.after.always(after);

// Create fixtures before each test
test.beforeEach(async (t) => {
  const testDataSize = 10;
  const testData = [];
  for (let i = 0; i < testDataSize; i = +1) {
    const article = {
      title: faker.company.catchPhrase(),
      author: faker.name.findName(),
      date: faker.date.past().toISOString(),
      text: faker.lorem.paragraphs(),
      tags: faker.random.array(0, 10, () => faker.random.word()),
      views: faker.random.number(),
      comments: faker.random.array(0, 10, () => faker.random.words()),
      scores: faker.random.array(0, 10, () => faker.random.number({ min: 0, max: 5 })),
    };
    testData.push(article);
  }

  for (let i = 0; i < testDataSize; i = +1) {
    const article = new Article(testData[i]);
    await article.save();
  }
  testData.forEach((item) => { item.slug = slug(item.title); });
  t.context.app = app;
  t.context.testData = testData;
});

// Helper from https://github.com/Marak/faker.js/issues/399#issuecomment-423534386
faker.random.array = function randomArray(min, max, obj) {
  const len = faker.random.number({ min, max });
  const array = [];

  for (let i = 0; i < len; i = +1) {
    array[i] = typeof obj === 'function' ? obj(i) : merge({}, obj);
  }

  return array;
};
