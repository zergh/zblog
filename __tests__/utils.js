const MongodbMemoryServer = require('mongodb-memory-server').default;
const mongoose = require('mongoose');

// Your models and server
const Article = require('../models/Article');
const app = require('../app');

const mongod = new MongodbMemoryServer();

// Create connection to mongoose before all tests
exports.before = async t => mongoose.connect(
  await mongod.getConnectionString(), { useNewUrlParser: true },
);

// Disconnect MongoDB and mongoose after all tests are done
exports.after = async (t) => {
  mongoose.disconnect();
  mongod.stop();
};
