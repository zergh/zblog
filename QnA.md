Вопросы
- Зачем задавал имя .env файлу? (variales.env) можно просто как .env задать.
//vargant
********** BACKEND *************

Замечания:

1. Конфигурация проекта.
Замечания:

- process.env используется для конфигурации и его применение разбросанно по разным файлам в проекте:
  -- конфигурацию лучше держать в одном файле (и там уже присваивать переменные окружения)
  -- проблемы подхода становяться очевидны если приложение будет требовать большего объема параметров для
  работы, что приведёт к появлению файла конфигурации, который будет использован наряду с process.env
  также надо учитывать конфигурации для деплоймента и различных CI/CD агентов (которые могут появиться)

- variable.env никогда не помещается в repository, причины:
  -- может содержать Environment Sensetive данные валидиные лишь для локали.
  -- может содержать секретные данные (такие данные не храняться в репозитории по причинам безопасности).
  -- такие файлы как правило не используются для продакшена.

- package.json
  -- main должен ссылаться на актуальный для запуска приложения entrypoint (/src/start.js)
  -- также советую не использовать OS-related пути (особенно от диска), лучше установить через chocholaty mongo
  или добавить mongo в PATH для доступа из терминала и использовать без указания пути к исполняемому файлу напрямую.

- ESLint
  -- Наследуйся и адаптируй конфигурации используемые повсеместно (airbnb, uber, facebook, extended или др.)

- Logging
  -- не используй console для замены логгера и не забывай подчищать за собой код от логгов которые используешь для
  эксперементов (тоже касается debugger; комманд)

IN Process:

2. Структура проекта:
3. Работа с Routing Layer, MiddleWares
4. Работа с БД
5. Работа с бизнес логикой
6. Обработка ошибок
7. Асинхронный аспект.

******************************************************************************************************

Возможные Решения:

- добавить в .gitignore .env файл

- добавить автора в package.json
- исправить путь к исполняему файлу в package.json

- добавить README.md для описания env variables которые ожидает приложение
(таблицей, формата: NAME, TYPE, DESCRIPTION, ниже приведу пример);
также описать команды в readme что нужно для запуска проекта
и описать важные npm команды

- для конфига приложения создать на каждую среду по файлу конфигурации (https://github.com/lorenwest/node-config)

- использовать prod-ready логгер (возможно с файл-роттатором) (
  см:
    * https://github.com/winstonjs/winston + https://github.com/winstonjs/winston-daily-rotate-file
    * https://github.com/trentm/node-bunyan
  отдельно:
    + https://github.com/expressjs/morgan (но этот для просмотра request/response)
  )

******************************************************************************************************
******************************************************************************************************
******************************************************************************************************
update
******************************************************************************************************
******************************************************************************************************
******************************************************************************************************
Вопросы
- Зачем задавал имя .env файлу? (variales.env) можно просто как .env задать.

********** BACKEND *************

Замечания:

1. Конфигурация проекта.
Замечания:

- process.env используется для конфигурации и его применение разбросанно по разным файлам в проекте:
  -- конфигурацию лучше держать в одном файле (и там уже присваивать переменные окружения)
  -- проблемы подхода становяться очевидны если приложение будет требовать большего объема параметров для
  работы, что приведёт к появлению файла конфигурации, который будет использован наряду с process.env
  также надо учитывать конфигурации для деплоймента и различных CI/CD агентов (которые могут появиться)

- variable.env никогда не помещается в repository, причины:
  -- может содержать Environment Sensetive данные валидиные лишь для локали.
  -- может содержать секретные данные (такие данные не храняться в репозитории по причинам безопасности).
  -- такие файлы как правило не используются для продакшена.

- package.json
  -- main должен ссылаться на актуальный для запуска приложения entrypoint (/src/start.js)
  -- также советую не использовать OS-related пути (особенно от диска), лучше установить через chocholaty mongo
  или добавить mongo в PATH для доступа из терминала и использовать без указания пути к исполняемому файлу напрямую.

- ESLint
  -- Наследуйся и адаптируй конфигурации используемые повсеместно (airbnb, uber, facebook, extended или др.)

- Logging
  -- не используй console для замены логгера и не забывай подчищать за собой код от логгов которые используешь для
  эксперементов (тоже касается debugger; комманд)

******************************************************************************************************

Возможные Решения:

- добавить в .gitignore .env файл

- добавить автора в package.json
- исправить путь к исполняему файлу в package.json

- добавить README.md для описания env variables которые ожидает приложение
(таблицей, формата: NAME, TYPE, DESCRIPTION, ниже приведу пример);
также описать команды в readme что нужно для запуска проекта
и описать важные npm команды

- для конфига приложения создать на каждую среду по файлу конфигурации (https://github.com/lorenwest/node-config)

- использовать prod-ready логгер (возможно с файл-роттатором) (
  см:
    * https://github.com/winstonjs/winston + https://github.com/winstonjs/winston-daily-rotate-file
    * https://github.com/trentm/node-bunyan
  отдельно:
    + https://github.com/expressjs/morgan (но этот для просмотра request/response)
  )

===========================================================================================

2. Работа с Routing Layer, MiddleWares:
Замечания:
- Не дублировать код для для разных методов без причины (особенно в случае создания ресурса).
Использовать Post либо Put.
- Рассмотреть использования Rest API Notation
- регистрация всех routes в одном файле может быть затруднительной со временем, как правило лучше разделять API
блоки по смыслу и функциям т.е в контексты (пр. в секции возможных решений)
- catchErrors не нужен, т.к может быть заменен на express global error handler (см. пункт 5)
```javascript
// https://expressjs.com/en/guide/error-handling.html
// Default error handler
app.use(function (err, req, res, next) {
  res.status(500).send('Something went wrong!')
})
```

******************************************************************************************************

Возможные Решения:
- Rest API - использовать HTTP Verbs для того чтоб описывать операции для разных маршрутов
  к примеру:
```javascript
const userRouter = express.Router();

module.exports = {
  register(app) {
    userRouter.get('/', userController.index);
    userRouter.get('/:id', userController.get);

    userRouter.post('/', userController.create);
    userRouter.put('/:id', userController.update);
    
    userRouter.delete('/:id', userController.update);

    app.use('/user', userRouter);
  }
}
```
POST - создание ресурса
PUT - обновление-замена либо создание ресурса
PATCH - обновление-модификация ресурса
GET - получения ресурса
DELETE - удаление ресурса 
* и др. (https://www.restapitutorial.com/lessons/httpmethods.html) *
под обновлением-модификацией

- регистрация маршрутов может быть сделана так 
(что в целом позволяет настраивать middlewares гибко вплоть до захвата целого блока)

```javascript
// # src/app.js
const routes = require('./routes');

...

routes.register(app);
```

```javascript
// # routes/index.js
const blogRoutes = require('./blog.js');

module.exports = {
  register(app) {
    blogRoutes.register(app);
    // otherRoutes.register(app);
  };
}

```

```javascript
// # routes/blog.js
module.exports = {
  register(app) {
    blogRouter.get('/', blogController.index);
    blogRouter.get('/:id', blogController.get);

    blogRouter.post('/', blogController.create);
    blogRouter.put('/:id', blogController.update);
    
    blogRouter.delete('/:id', blogController.update);

    app.use('/user', blogRouter);
  }
}
```

===========================================================================================

3. Работа с БД
4. Работа с бизнес логикой
5. Обработка ошибок

6. Асинхронный аспект.
Отметки:
- Обсудить ошибки в gulp files, runCommand, paths

7. Структура проекта:

