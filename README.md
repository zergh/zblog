# zblog

Patchy work in progress at best. Check __docs__ folder for details.

## Installation

Prerequisites: Node v.11 and Python 2.7 (for gulp)

```bash
npm i
```

Note: only dev environment supported.
Note: eslint is local, launches like

```bash
node G:\\dev\\zblog\\node_modules\\eslint\\bin\\eslint gulpfile.js --fix
```

## Launch

Your build should be [here](http://localhost:4200/), by default.

```bash
npm start
```

Or

```bash
gulp
```

## Logging

Logging engine is outside of the app code. You'll need to install it on the machine running the code.

```
npm i -g pino-pretty pino-tee
```

This prints logs to console

```
gulp | pino-pretty 
```


This saves logs to files. You do a lot to the logs, here is the [Pino ecosystem overview](http://getpino.io/#/docs/ecosystem)

```
gulp | pino-tee warn .__logs__/warn-logs > .__logs__/all-logs
```

## Test

```bash
npm test
```

## Misc




Very raw for the moment, working on skeleton setup.

Also, should switch from pug to normal FE. And should cook the API first.