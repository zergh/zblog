const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const notify = require('gulp-notify');
const { exec } = require('child_process');
const spawn = require('cross-spawn');
const eslint = require('gulp-eslint');
const ava = require('gulp-ava');
const config = require('config');
const logger = require('pino')()

// Node configurations
const dbCfg = config.get('Gulp.dbConfig');
const pathCfg = config.get('Gulp.pathConfig');

gulp.task('eslint', () => gulp
  .src(['src/**/*.js'])
  .pipe(eslint({fix:true}))
  .pipe(eslint.format())
  //.pipe(eslint.failAfterError())
);

gulp.task('test', () => gulp.src('src/__tests__')
  .pipe(ava({ verbose: true })));

gulp.task('notify', () => gulp
  .pipe(notify(`Running at ${new Date().toUTCString()}`)));

gulp.task('nodemon', (done) => {
  const stream = nodemon({
    script: pathCfg.input.starter_script,
    ext: 'js',
    watch: ['src/*', 'src/*/**'],
    tasks: ['eslint'], //'test'
    stdout: true,
    readable: false,
    done,
  });
  stream
    .on('restart', () => {
//      log.info('Application has restarted');
    })
    .on('crash', () => {
 //     log.error('Application has crashed!\n');
      stream.emit('restart', 10); // restart the server in 10 seconds
    });
});

gulp.task('default', gulp.series('eslint', 'nodemon')); // 'eslint', 'test',
