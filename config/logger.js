const pino = require('pino');

//import { APP_ID, LOG_LEVEL } from "./Config";

const logger = pino({
  name: 'z-app-name',
  level: 'debug'
});