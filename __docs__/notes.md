# TODO

## Mongo launcher needed

```javascript
gulp.task('launch_mongo', () => {
  const options = {
    continueOnError: false, // default = false, true means don't emit error event
    pipeStdout: false, // default = false, true means stdout is written to file.contents
    customTemplatingThing: 'test', // content passed to lodash.template()
  };
  const reportOptions = {
    err: true, // default = true, false means don't write err
    stderr: true, // default = true, false means don't write stderr
    stdout: true, // default = true, false means don't write stdout
  };
  return gulp.src('./**/**')
    .pipe(exec('mkdir g:\\mongodb\\data\\db', options))
    .pipe(exec('mkdir g:\\mongodb\\logs', options))
    .pipe(exec('mkdir g:\\mongodb\\config', options))
    .pipe(exec('port=27017 \
            dbpath=C:\\mongodb\\data\\db\\ \
            logpath=C:\\mongodb\\logs\\mongod.log \
            maxConns=100\
            ' > 'g:\\mongodb\\config\\mongodb.cfg', options))
    .pipe(exec(`${config.path.database_launcher} --directoryperdb --config "g:\\mongodb\\config\\mongodb.cfg" --install --serviceName "MongoDB" net start MongoDB`, options))
    .pipe(exec.reporter(reportOptions));
});
```

## Add SCSS gulp jobs when front-end comes up

```javascript
const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');
const gulpIf = require('gulp-if');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const gulpStylelint = require('gulp-stylelint');


gulp.task('fix-scss', () => gulp
  .src('src/**/*.scss')
  .pipe(gulpStylelint({
    fix: true,
    failAfterError: false,
  }))
  .pipe(gulp.dest('src'))
);

gulp.task('stylelint', () => gulp
  .src('src/**/*.scss')
  .pipe(gulpStylelint({
    failAfterError: false,
    reporters: [
      { formatter: 'string', console: true },
    ],
  }))
);

gulp.task('cook-scss', () => gulp
  .src(config.path.scss)
  .pipe(gulpIf(config.isDevelop, sourcemaps.init()))
  .pipe(sass())
  .pipe(concat(config.output.cssName))
  .pipe(autoprefixer())
  .pipe(gulpIf(!config.isDevelop, cleanCss()))
  .pipe(gulpIf(config.isDevelop, sourcemaps.write()))
  .pipe(gulp.dest(config.output.path))
);

gulp.task('watch_scss', () => gulp
  .watch(config.path.scss, gulp.series('fix-scss', 'stylelint', 'cook_scss'))
);
```
